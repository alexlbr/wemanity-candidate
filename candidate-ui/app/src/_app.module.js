(function(){
    'use strict';

    angular
        //REMOVE THE app.backMock DEPENDENCY TO CONNECT THE FRONT TO THE BACK
        .module('app', ['app.backMock', 'ngAnimate','ui.router', 'ui.bootstrap' ,'app.home', 'app.step','app.candidate'])
        .config([
            '$stateProvider', '$urlRouterProvider',
            AppConfig
        ])
        .run(['$templateCache', '$http', 
            AppRun
        ]);

    function AppConfig($stateProvider, $urlRouterProvider) {

        // Route
        $urlRouterProvider.otherwise('/home');

        $stateProvider.state('app',{
            abstract    : true,
            templateUrl : 'src/main.template.html'
        });
    }

    function AppRun($templateCache, $http) {

        $http.get('src/error-messages.html')
            .then(function(response) {
                $templateCache.put('error-messages', response.data); 
        });
    }

})();
