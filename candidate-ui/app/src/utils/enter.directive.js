(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('appEnter',[
            AppEnter
        ]);

    //directive
    function AppEnter() {
        
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.appEnter);
                    });
     
                    event.preventDefault();
                }
            });
        };

    }

})();