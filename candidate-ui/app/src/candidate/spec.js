describe('Candidate controller', function() {

	beforeEach(module('app'));

	var CandidateController,
	scope,
	candidates;

	beforeEach(inject(function ($rootScope, $controller, $filter, CandidateService) {
		scope = $rootScope.$new();
		CandidateController = $controller('CandidateController', {
			$scope: scope,
			$filter: $filter,
			CandidateService: CandidateService 
		});
		candidates = CandidateController.candidates.length;
	}));

	it('by default sorts by name', function () {
		expect(CandidateController.sortType).toEqual("name");
	});

});