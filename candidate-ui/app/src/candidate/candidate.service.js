(function(){
    'use strict';

    angular
        .module('app.candidate')
        .service('CandidateService', [
            '$http',
            CandidateService
        ]);


    function CandidateService($http) {
        var self = this;

        this.get = get;
        this.put = put;
        this.post = post;
        this.remove = remove;

        function get(){
            return $http.get('/candidate');
        }

        function put(candidate){
            return $http.put('/candidate/'+candidate.id, { enabled : candidate.enabled}).then(function(response){

                console.log(response); 
                return response;
            });
        }

        function post(candidate){
            return $http.post('/candidate', candidate).then(function(response){

                console.log(response); 
                return response;
            });
        }

        function remove(candidates){
            return $http.post('/candidate/delete', candidates).then(function(response){

                console.log(response); 
                return response;
            });
        }

    }

})();
