(function(){
    'use strict';

    angular
        .module('app.candidate',['ngMessages'])
        .config([
            '$stateProvider',
            CandidateConfig
        ]);

    function CandidateConfig($stateProvider) {

        $stateProvider.state('app.candidates',{
            url : '/candidate',
            views : {
                'main@app' : {
                    templateUrl : 'src/candidate/candidates.html',
                    controller : 'CandidateController as ctrl'
                }
            },
            data: {

            }
        });
    }

})();
