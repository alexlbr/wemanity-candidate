(function(){
    'use strict';

    angular
        .module('app.candidate')
        .controller('CandidateController', [ 
            '$scope',
            '$filter',
            'CandidateService',
            CandidateController
        ]);

    function CandidateController($scope, $filter, CandidateService) {
        var self = this;
        
        self.sortType     = 'name'; // set the default sort type
        self.sortReverse  = false;  // set the default sort order
        self.candidates = [];
        self.newCandidate = { name : '', enabled : true };
        self.selected = [];

        //hiding implementation details bellow and making it easy to read public methods
        self.addCandidate = addCandidate;
        self.removeCandidate = removeCandidate;
        self.toggleSelection = toggleSelection; 

        activate();

        function activate() {

            //initializing the candidates with data from the service
            CandidateService.get().then(function(response){
                self.candidates = response.data;
            });
        }

        function addCandidate() {

            //calling the service to add a new candidate in the backend
            CandidateService.post(self.newCandidate).then(function(response){

                //updating the model so the view gets updated
                self.candidates.push(response.data);
                self.newCandidate.name = '';

                //reseting the form
                if($scope.candidateForm) $scope.candidateForm.$setPristine();
            })
        }   

        function removeCandidate() {

            if (self.selected.length == 0)
                return;

            //calling the service to update the backend
            CandidateService.remove(self.selected).then(function(response){

                //updating the model so the view gets updated
                self.candidates = $filter('filter')(self.candidates, function(candidate){
                    
                    return (self.selected.indexOf(candidate.id) < 0) 
                });
            })
        }

        function toggleSelection(id) {

            var index = self.selected.indexOf(id);

            // is currently selected
            if (index > -1) {
              self.selected.splice(index, 1);
            }

            // is newly selected
            else {
              self.selected.push(id);
            }
        }
    }

})();
