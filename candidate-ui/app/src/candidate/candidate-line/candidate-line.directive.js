(function() {
    'use strict';

    angular
        .module('app.candidate')
        .directive('candidateLine', [
            'CandidateService',
            CandidateLine
        ]);

    //directive
    function CandidateLine(CandidateService) {
        return {
            bindToController : true,
            controller: function() {

                this.updateEnabled = function(candidate) {

                    CandidateService.put(candidate);
                }
            },
            controllerAs : 'ctrl',
            replace : false,
            restrict : 'EA',
            scope : { 
                candidate : '=',
                toggleSelection : '&'
            },
            templateUrl : 	'./src/candidate/candidate-line/candidate-line.html'
        };
    }

})();
